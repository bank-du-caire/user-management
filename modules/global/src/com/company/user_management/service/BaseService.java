package com.company.user_management.service;

import com.company.user_management.entity.UserRole;
import com.company.user_management.entity.Users;
import com.haulmont.cuba.security.entity.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface BaseService {
    String NAME = "usermanagement_BaseService";

    public List<com.company.user_management.entity.UserRole> loadUserRoles(Users user);

    Users addEditUser(Users user, List<UserRole> UserRoles);

    User addEditSecUser(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    List<com.haulmont.cuba.security.entity.UserRole> CheckSecUserRoleDel(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    User updateSecUserAndRoles(User user, List<com.haulmont.cuba.security.entity.UserRole> UserRoles);

    List<UserRole> CheckuserRoleDel(Users user, List<UserRole> UserRoles);

    Users updateUserAndRoles(Users user, List<UserRole> UserRoles);

    List<Date> getLastLogoutDate(UUID id);
}