-- begin USERMANAGEMENT_USER_ROLE
create table USERMANAGEMENT_USER_ROLE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    USERS_ID varchar(36),
    ROLE_ID varchar(36),
    ROLE_NAME varchar(255),
    --
    primary key (ID)
)^
-- end USERMANAGEMENT_USER_ROLE
-- begin USERMANAGEMENT_USERS
create table USERMANAGEMENT_USERS (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LOGIN varchar(255) not null,
    LAST_LOGIN_DATE timestamp,
    STATUS varchar(50),
    GROUP_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end USERMANAGEMENT_USERS
