-- begin USERMANAGEMENT_USER_ROLE
create table USERMANAGEMENT_USER_ROLE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    USERS_ID varchar2(32),
    ROLE_ID varchar2(32),
    ROLE_NAME varchar2(255 char),
    --
    primary key (ID)
)^
-- end USERMANAGEMENT_USER_ROLE
-- begin USERMANAGEMENT_USERS
create table USERMANAGEMENT_USERS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    LOGIN varchar2(255 char) not null,
    LAST_LOGIN_DATE timestamp,
    STATUS varchar2(50 char),
    GROUP_ID varchar2(32) not null,
    ACTIVE char(1),
    ALL_ROLES_IN_STRING varchar2(1000 char),
    --
    primary key (ID)
)^
-- end USERMANAGEMENT_USERS
