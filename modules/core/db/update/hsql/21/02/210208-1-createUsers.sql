create table USERMANAGEMENT_USERS (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    LOGIN varchar(255) not null,
    LAST_LOGIN_DATE timestamp,
    STATUS varchar(50),
    GROUP_ID varchar(36) not null,
    --
    primary key (ID)
);